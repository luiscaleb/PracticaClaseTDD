﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTDD
{
   public class Respuesta
    {
        public string Mensaje { get; set; }
        public bool Error { get; set; }
        public decimal Saldo { get; set; }
    }
}
