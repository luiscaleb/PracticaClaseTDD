﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTDD
{
    public abstract class CuentaBancaria
    {
        public decimal Saldo { get; set; }
        public abstract Respuesta Consignar(decimal valor);
        public abstract Respuesta Retirar(decimal valor);
    }
}
