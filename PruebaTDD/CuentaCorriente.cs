﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTDD
{
    public class CuentaCorriente : CuentaBancaria
    {
        Respuesta respuesta = new Respuesta();
        public override Respuesta Consignar(decimal Valor)
        {
            if (Valor <= 0)
            {
                respuesta.Mensaje = "No se puede realizar la consignacion";
                respuesta.Saldo = Saldo;
            }
            else
            {
                Saldo = Saldo + Valor;
                respuesta.Mensaje = "Consignacion exitosa...!";
                respuesta.Saldo = Saldo;
            }
            return respuesta;
        }

        public override Respuesta Retirar(decimal Valor)
        {
            if (((Saldo - Valor) < -10000) || valoresMenoresCero(Valor))
            {
                respuesta.Mensaje = "No se puede realizar el retiro";
                respuesta.Saldo = Saldo;
            }
            else {
                Saldo = Saldo - Valor;
                respuesta.Mensaje = "Retiro Correcto";
                respuesta.Saldo = Saldo;
            }
            return respuesta;
            
        }

        public bool valoresMenoresCero(decimal Valor)
        {
            bool resp = false;
            if (Valor <= 0)
            {
                resp = true;
            }
            return resp;
        }
    }
}
