﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PruebaTDD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTDD.Tests
{
    [TestClass()]
    public class CuentaCorrienteTests
    {
        [TestMethod()]
        public void RetirarCorrectamenteTest()
        {
            CuentaCorriente c = new CuentaCorriente();
            c.Saldo = 1000;
            Respuesta respuesta =  c.Retirar(11000);
            Assert.AreEqual("Retiro Correcto", respuesta.Mensaje);
            Assert.AreEqual(-10000, respuesta.Saldo);
        }

        [TestMethod()]
        public void RetirarInorrectamenteTest()
        {
            CuentaCorriente c = new CuentaCorriente();
            c.Saldo = 1000;
            Respuesta respuesta = c.Retirar(11001);
            Assert.AreEqual("No se puede realizar el retiro", respuesta.Mensaje);
            Assert.AreEqual(1000, respuesta.Saldo);
        }

        [TestMethod()]
        public void RetirarValorMenorIgualCeroTest()
        {
            CuentaCorriente c = new CuentaCorriente();
            c.Saldo = 1000;
            Respuesta respuesta = c.Retirar(-10);
            Assert.AreEqual("No se puede realizar el retiro", respuesta.Mensaje);
            Assert.AreEqual(1000, respuesta.Saldo);
        }

        [TestMethod()]
        public void ConsignarCorrectamenteTest()
        {
            CuentaCorriente c = new CuentaCorriente();
            c.Saldo = 1000;
            Respuesta respuesta = c.Consignar(30000);
            Assert.AreEqual("Consignacion exitosa...!", respuesta.Mensaje);
            Assert.AreEqual(31000, respuesta.Saldo);
        }

        [TestMethod()]
        public void ConsignarInorrectamenteTest()
        {
            CuentaCorriente c = new CuentaCorriente();
            c.Saldo = 1000;
            Respuesta respuesta = c.Consignar(-10);
            Assert.AreEqual("No se puede realizar la consignacion", respuesta.Mensaje);
            Assert.AreEqual(1000, respuesta.Saldo);
        }

    }
}